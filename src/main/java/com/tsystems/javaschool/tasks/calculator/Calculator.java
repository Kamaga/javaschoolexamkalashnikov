package com.tsystems.javaschool.tasks.calculator;

import java.util.Stack;

public class Calculator {

    public String evaluate(String statement) {
        //below i'll transform expression into a Polish Reverse Notation (https://en.wikipedia.org/wiki/Reverse_Polish_notation)
        //using a Shunting-yard algorithm (https://en.wikipedia.org/wiki/Shunting-yard_algorithm)


        String answer;
        if (isOperationSingsInCorrectSequence(statement) && isParenthesesInCorrectSequence(statement)) {


            if (getAnswerFromReversed((getReversed(statement))) == Double.POSITIVE_INFINITY ||
                    getAnswerFromReversed((getReversed(statement))) == Double.NEGATIVE_INFINITY)
                return null;

            else
                answer = Double.toString(getAnswerFromReversed(getReversed(statement)));
            return roundAndDeleteZeroAfterDot(answer);
        } else return null;
    }

    private static boolean isParenthesesInCorrectSequence(String statement) {
        int validCheck = 0;
        boolean isSequenceIsCorrect = true;
        for (char c : statement.toCharArray()) {
            if (c == '(') {validCheck++;}
            else if (c == ')') {validCheck--;}
            if (validCheck != 0) {isSequenceIsCorrect = false;}
            else {isSequenceIsCorrect = true;}
        }
        return isSequenceIsCorrect;

    }

    private static boolean isOperationSingsInCorrectSequence(String statement) {
        if (statement == null)
            return false;


        if (statement.isEmpty() ||
                statement.startsWith("+") ||
                statement.startsWith("*") ||
                statement.startsWith("/") ||
                statement.startsWith("-(") ||
                statement.contains("./") ||
                statement.contains("++") ||
                statement.contains("--") ||
                statement.contains("**") ||
                statement.contains("//") ||
                statement.contains("..") ||
                statement.contains("+-") ||
                statement.contains("-+") ||
                statement.contains("+*") ||
                statement.contains("*+") ||
                statement.contains("+/") ||
                statement.contains("/+") ||
                statement.contains("+.") ||
                statement.contains(".-") ||
                statement.contains("-*") ||
                statement.contains("*-") ||
                statement.contains("-/") ||
                statement.contains("/-") ||
                statement.contains("-.") ||
                statement.contains("*/") ||
                statement.contains("/*") ||
                statement.contains("*.") ||
                statement.contains(".*") ||
                statement.contains(",") ||
                statement.matches(".*[a-zA-Z].*"))

        {return false;}
        else {return true;}
    } //that is stupid but it works

    private static int priority(char symbol) {

        if (symbol == '*' || symbol == '/') return 3;
        else if (symbol == '+' || symbol == '-') return 2;
        else if (symbol == '(') return 1;
        else if (symbol == ')') return -1;
        else return 0;
    }

    private static String getReversed(String expression) {
        StringBuilder expressionBuilder = new StringBuilder();
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < expression.length(); i++) {

            if (priority(expression.charAt(i)) == 0) expressionBuilder.append(expression.charAt(i));

            if (priority(expression.charAt(i)) == 1) stack.push(expression.charAt(i));

            if (priority(expression.charAt(i)) == -1) {
                expressionBuilder.append(" "); // space to separate the operands
                while (priority(stack.peek()) != 1) expressionBuilder.append(stack.pop());
                stack.pop();
                //to remove parentheses we getting out of stack all the operations signs till we find the opening
            }
            if (priority(expression.charAt(i)) == 2 || priority(expression.charAt(i)) == 3) {
                expressionBuilder.append(" ");
                while (!stack.empty()) {
                    if (priority(stack.peek()) >= priority(expression.charAt(i))) expressionBuilder.append(stack.pop());
                    else break;
                }
                stack.push(expression.charAt(i));
            }
        }
        //pushing operation signs in a right sequence as its priorities

        while (!stack.empty()) expressionBuilder.append(stack.pop()); //to complete reversed expression

        return expressionBuilder.toString();
    }

    private static double getAnswerFromReversed(String reversed) {
        StringBuilder operandBuilder = new StringBuilder();
        Stack<Double> stack = new Stack<>();
        for (int i = 0; i < reversed.length(); i++) {
            if (reversed.charAt(i) == ' ') continue;
            if (priority(reversed.charAt(i)) == 0) {
                while (reversed.charAt(i) != ' ' && priority(reversed.charAt(i)) == 0) {
                    operandBuilder.append(reversed.charAt(i));
                    i++;
                    if (i == reversed.length()) break;

                }
                stack.push(Double.parseDouble(operandBuilder.toString()));
                operandBuilder = new StringBuilder();
            }
            //get operands before the operations signs and push to stack
            if (priority(reversed.charAt(i)) > 1) {
                double a = stack.pop();
                double b = stack.pop();
                if (reversed.charAt(i) == '+') stack.push(b + a);
                if (reversed.charAt(i) == '-') stack.push(b - a);
                if (reversed.charAt(i) == '*') stack.push(b * a);
                if (reversed.charAt(i) == '/') stack.push(b / a);
            }
            //find operations signs, calculate two top operands and push the result back in stack
        }
        return stack.pop();
        //after loop the last value in stack is the answer
    }

    private static String roundAndDeleteZeroAfterDot(String answer) {
        StringBuilder answerBuilder = new StringBuilder();
        answerBuilder.append(answer);
        boolean isInt = true;
        int indexOfDot = 0;
        for (int i = 0; i < answer.length(); i++) {
            if (answer.charAt(i) == '.') indexOfDot = i;
        }
        //find an index of dot
        for (int i = indexOfDot + 1; i < answer.length(); i++) {
            if (answer.charAt(i) != '0') {

                isInt = false;
            } else isInt = true;

        }
        //decide is the answer int or double
        if (isInt) answerBuilder.delete(indexOfDot, answer.length() + 1);
            //delete digits and dot if result is int
        else
            for (int i = indexOfDot + 5; i < answer.length(); i++) {
                answerBuilder.deleteCharAt(i);
            }
        //round answer if result is double
        return answerBuilder.toString();

    }

}



