package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;
import java.util.Stack;

public class Subsequence {

    public boolean find(List x, List y) throws IllegalArgumentException {
        if (x == null || y == null)
        {
            throw new IllegalArgumentException();
        }
        if (x.size() == 0) {
            return true;
        }
        Stack<Object> xStack = new Stack<>();
        Stack<Object> yStack = new Stack<>();
        Object elementsOfX, elementsOfY;
        for (Object xTemp : x) xStack.push(xTemp);
        for (Object yTemp : y) yStack.push(yTemp);// to complete the stacks with our lists
        elementsOfX = xStack.pop();
        while (!yStack.empty()) {
            elementsOfY = yStack.pop();
            if (elementsOfX.equals(elementsOfY)) {
                if (xStack.empty()) {
                    return true; // if during compering xStack is out of elements Y is a part of X;
                }
                elementsOfX = xStack.pop();
            }
        }
        return false;
    }
}


