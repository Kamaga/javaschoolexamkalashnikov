package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {
    public int[][] buildPyramid(List<Integer> inputNumbers) {

        if (inputNumbers.contains(null) || inputNumbers.size() >= 255)
            throw new CannotBuildPyramidException();
        Collections.sort(inputNumbers);
        int pyramidHeight = 0;
        int size = inputNumbers.size();
        for (int i = 0; i < inputNumbers.size(); i++) {
            if (inputNumbers.get(i) != null) {
                size -= i;
                if (size > 0) {
                    pyramidHeight++;
                }


            }
        } //arrays height

        int current = 0;
        int pyramidWidht;
        pyramidWidht = pyramidHeight * 2 - 1; // arrays width
        int[][] pyramid = new int[pyramidHeight][pyramidWidht];//now we don't need ad input statement because we can
        //build a pyramid just with height and width;
        for (int i = 0; i < pyramidHeight; i++) {
            int a = pyramidHeight - i - 1;
            for (int j = 0; j <= i; j++) {
                pyramid[i][a] = inputNumbers.get(current);
                current++;
                a += 2;
            }
        }

        return pyramid;
    }
}

